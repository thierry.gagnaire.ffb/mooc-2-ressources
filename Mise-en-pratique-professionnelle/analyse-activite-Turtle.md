# Analyse Activité Turtle

- Objectifs :
  - objectif annoncé par le concepteur : introduire Python
  - Turtle peut être utilisé pour faire le lien entre un langage classique comme Python et un langage éducatif comme Scratch (si le public cible est constitué de lycéens - qui ont en principe utilisé Scratch au collège)
  - objectif possible : introduction aux boucles
  - décomposition de problèmes en sous-problèmes, modularité (répétition de motifs)
  - objectifs mathématiques : constructions géométriques, géométrie plane dans un repère, théorème de Pythagore, ...
  - objectif possible : recherche dans la documentation d'une bibliothèque standard de Python
- Pré-requis à cette activité :
  - cours sur les fonctions et les boucles en Python (, éventuellement : import d'un module ou d'une bibliothèque)
  - notions mathématiques : géométrie et repère orthonormé, angles... (qu'est-ce qu'un "double décagone" ?)
  - lancement d'un interpréteur Python
  - connaissance de la fenêtre où apparaît le résultat
  - documentation Turtle, ou cours, ou informations sur certaines fonctions de Turtle (dessiner un cercle, aller à une position, relever le stylo, ...)
- Durée de l'activité : 3h30
- Exercices cibles :
  - pour les élèves les plus avancés : après les premiers essais, réaliser les figures de manière efficace, c'est-à-dire en utilisant le moins d'instructions possibles, des fonctions réutilisables...
  - NB : cette activité "Turtle" est déjà centrée sur la réalisation d'exercices
- Description du déroulement de l'activité :
  - cours sur les pré-requis manquants : 30mn
  - le premier exercice du niveau 1 - directement dans l'interpréteur : 5mn
  - les 4 exercices suivants du niveau 1 - en sauvegardant dans un fichier python : 25mn
  - point sur l'avancement des élèves, et instructions pour le niveau 2 : 15mn
  - les 6 exercices du niveau 2 : 25mn
  - instructions données au fil de l'eau pour les niveaux 3 et 4 : 5mn
  - les 5 exercices du niveau 3 : 30mn
  - les 4 exercices du niveau 4 : 40mn
  - point de synthèse dirigé par l'enseignant : 15mn
  - corrigé type et commentaires : 20mn (+ éventuellement support des cours suivants - exemple = "modularité" : 1h à 3h)
- Anticipation des difficultés des élèves :
  - Préciser qu'il ne faut pas donner 'turtle.py' au nom du fichier de réalisation des exercices
  - Instructions générales pour indiquer les méthodes à utiliser, les options de réalisation quand plusieurs méthodes sont possibles : faut-il revenir sur des pas, faut-il aller directement à une position, les polygones peuvent-ils être dessinés par une fonction prédéfinie de la bibliothèque, ... ?
  - Indiquer le nombre de décimales utilisables dans ce plan géométrique
- Gestion de l'hétérogénéïté
  - remédiation pour les élèves en difficulté (au niveau 1 ou 2) : dessiner la figure cible sur une feuille et écrire un algorithme en langage naturel décrivant les opérations effectuées
  - laisser les élèves aller à leur rythme, peu importe où chacun en sera à la fin
  - exercices supplémentaires pour les élèves en avance :
    - option 1 : passer en paramètre la taille des figures, et essayer différentes tailles
    - option 2 : écrire (ou réécrire) le code de génération des figures de manière modulaire
    - option 3 : spécifier eux-mêmes une nouvelle figure à réaliser, puis écrire le code

  
  