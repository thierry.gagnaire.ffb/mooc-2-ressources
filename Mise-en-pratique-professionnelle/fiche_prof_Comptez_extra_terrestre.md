# Activité : "Comptez comme un extra-terrestre !"

**Thématique :** Représentation des données : les types simples

**Sous-Thème(s) :** Représentation des entiers naturels en machine, changement de base

**Niveau :** Classe de 1ere - spécialité NSI

**Pré-requis :**
- Mathématiques :
  - arithmétique du collège et de seconde (division euclidienne, modulo).
  - notions de table d'addition et table de multiplication (vues en primaire).
- Informatique : notions de programmation Python (abordées en seconde) : fonctions, boucles.

**Notions liées :** Les variables dans un langage de programmation ; adresses IP ; codage des couleurs RGB en photographie numérique

**Résumé de l’activité :** Activité débranchée pour les trois quarts (et application en Python à la fin)
- Découverte de différentes bases du système de numération de position, dont : octale, binaire, hexadécimale.

**Objectifs :** 
- Appropriation de la logique d'une base quelconque dans un système de numération de position.
- Acquisition de facilités de manipulation et de conversion des nombres en bases binaire, octale et hexadécimale (y compris pour les nombres avec une partie décimale).

**Auteur :** Thierry

**Durée de l’activité :** 3h (quatre parties d'envrion 45mn chacune)

**Forme de participation :** Individuelle (ou en binôme), collective (alternativement en autonomie - sans doute avec une aide supplémentaire) 

**Matériel nécessaire :** Papier et stylo ; puis un poste informatique (uniquement pour le dernier quart de l'activité en Python)

**Préparation :** Aucune

**Autres références :** 
- Eduscol - BO - Annexe "Programme de numérique et sciences informatiques de première générale"
  - [Site de l'éducation nationale](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
  - Section "Représentation des données : types et valeurs de base" ; extraits :
    - Contenus : Écriture d’un entier positif dans une base b >= 2.
    - Capacités attendues : Passer de la représentation d’une base dans une autre.
    - Commentaires : Les bases 2, 10 et 16 sont privilégiées.
- Site de Gisèle Bareux - page : [Représentation des données : types et valeurs de base](http://gisele.bareux.free.fr/NSI1/NSI_1iere_S1.pdf)

**Fiche élève cours :** 

**Fiche élève activité :** Disponible [ici](activite_Comptez_extra_terrestre.pdf)   
